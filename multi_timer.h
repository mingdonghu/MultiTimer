/*
 * Copyright (c) 2021 0x1abin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef _MULTI_TIMER_H_
#define _MULTI_TIMER_H_

#include <stdint.h>

#ifdef __cplusplus  
extern "C" {  
#endif

typedef struct multi_timer_link_list_t multi_timer_t;

typedef uint64_t (*platform_ticks_interface_t)(void);

typedef void (*multi_timer_callback_t)(multi_timer_t* timer, void* user_data);

struct multi_timer_link_list_t {  // 数据结构： 单链表
  uint64_t                        deadline;
  multi_timer_callback_t          callback;
  void*                           user_data;
  multi_timer_t*                  next;
};

/**
 * @brief Platform ticks function.
 * 
 * @param ticks_func ticks function.
 * @return int 0 on success, -1 on error.
 */
int multi_timer_install_ticks(platform_ticks_interface_t ticks_func);

/**
 * @brief Start the timer work, add the handle into work list.
 * 
 * @param timer target handle strcut.
 * @param timing Set the start time.
 * @param callback deadline callback.
 * @param user_data user data.
 * @return int 0: success, -1: fail.
 */
int multi_timer_start(multi_timer_t* timer, uint64_t timing, multi_timer_callback_t callback, void* user_data);

/**
 * @brief Stop the timer work, remove the handle off work list.
 * 
 * @param timer target handle strcut.
 * @return int 0: success, -1: fail.
 */
int multi_timer_stop(multi_timer_t* timer);

/**
 * @brief Check the timer expried and call callback.
 * 
 * @return int The next timer expires.
 */
int multi_timer_yield(void);

#ifdef __cplusplus
} 
#endif

#endif
