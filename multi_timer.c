#include "multi_timer.h"
#include <stdio.h>

/* Timer handle list head. */
static multi_timer_t* p_timer_list_ = NULL; // 全局指针变量

/* Timer tick */
static platform_ticks_interface_t platformTicksFunction = NULL;

int multi_timer_install_ticks(platform_ticks_interface_t ticks_func) {
  platformTicksFunction = ticks_func;
  return 0;
}

int multi_timer_start(multi_timer_t* timer, uint64_t timing, multi_timer_callback_t callback, void* user_data) {
  if (!timer || !callback ) {
    return -1;
  }

  multi_timer_t** p_next_timer_pointer_addr = &p_timer_list_;
  /* Remove the existing target timer. */
  for (; *p_next_timer_pointer_addr; p_next_timer_pointer_addr = &(*p_next_timer_pointer_addr)->next) {
    if (timer == *p_next_timer_pointer_addr) {
      *p_next_timer_pointer_addr = timer->next; /* remove from list */
      break;
    }
  }

  /* Init timer. */
  timer->deadline = platformTicksFunction() + timing;
  timer->callback = callback;
  timer->user_data = user_data;

  /* Insert timer. */ 
  // 前插法
  for (p_next_timer_pointer_addr = &p_timer_list_;; p_next_timer_pointer_addr = &(*p_next_timer_pointer_addr)->next) {
    if (!*p_next_timer_pointer_addr) {
      timer->next = NULL;
      *p_next_timer_pointer_addr = timer;
      //printf("header,timer:%p\r\n", timer);
      break;
    }

    if (timer->deadline < (*p_next_timer_pointer_addr)->deadline) {
      timer->next = *p_next_timer_pointer_addr;
      *p_next_timer_pointer_addr = timer;
      //printf("timer:%p\r\n", timer);
      break;
    }
  }

  return 0;
}

int multi_timer_stop(multi_timer_t* timer) {
  multi_timer_t** p_next_timer_pointer_addr = &p_timer_list_;

  /* Find and remove timer. */
  for (; *p_next_timer_pointer_addr; p_next_timer_pointer_addr = &(*p_next_timer_pointer_addr)->next) {
    multi_timer_t* entry = *p_next_timer_pointer_addr;
    if (entry == timer) {
      *p_next_timer_pointer_addr = timer->next;
      break;
    }
  }

  return 0;
}

int multi_timer_yield(void) {
  multi_timer_t* entry = p_timer_list_;

  for (; entry; entry = entry->next) {
    /* Sorted list, just process with the front part. */
    if (platformTicksFunction() < entry->deadline) {
      return (int)(entry->deadline - platformTicksFunction());
    }

    /* remove expired timer from list */
    p_timer_list_ = entry->next;

    /* call callback */
    if (entry->callback) {
      entry->callback(entry, entry->user_data);
    }
  }

  return 0;
}
