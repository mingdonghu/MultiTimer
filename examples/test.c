#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include "../multi_timer.h"
#include <unistd.h>

// global variable
multi_timer_t g_timer1_;
multi_timer_t g_timer2_;
multi_timer_t g_timer3_;

uint64_t platform_ticks_get_interface(void) {
  struct timespec current_time;
  clock_gettime(CLOCK_MONOTONIC, &current_time);
  return (uint64_t)((current_time.tv_sec * 1000) + (current_time.tv_nsec / 1000000)); // unit is millisecond.
}

void example_timer1_callback(multi_timer_t* timer, void *user_data) {
  printf("[%012ld] Timer:%p callback-> %s.\r\n", platform_ticks_get_interface(), timer, (char*)user_data);
  multi_timer_start(timer, 1000, example_timer1_callback, user_data);
}

void example_timer2_callback(multi_timer_t* timer, void *user_data) {
  printf("[%012ld] Timer:%p callback-> %s.\r\n", platform_ticks_get_interface(), timer, (char*)user_data);
}

void example_timer3_callback(multi_timer_t* timer, void *user_data) {
  printf("[%012ld] Timer:%p callback-> %s.\r\n", platform_ticks_get_interface(), timer, (char*)user_data);
  multi_timer_start(timer, 4567, example_timer3_callback, user_data);
}

int main(int argc, char *argv[]) {
  multi_timer_install_ticks(platform_ticks_get_interface);

  multi_timer_start(&g_timer1_, 1000, example_timer1_callback, "1000ms CYCLE timer");
  multi_timer_start(&g_timer2_, 5000, example_timer2_callback, "5000ms ONCE timer");
  multi_timer_start(&g_timer3_, 3456, example_timer3_callback, "3456ms delay start, 4567ms CYCLE timer");

  while (1) {
    multi_timer_yield();
    printf("----\n");
    usleep(1000 * 1000);
  }
}